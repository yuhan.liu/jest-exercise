import { sendBombSignal, dieTogether } from './nuclear-bomb';

test('请测试 - sendBombSignal 会向 bomb 函数传递 O_o 作为起爆指令', () => {
  const mockdemo = jest.fn();
  sendBombSignal(mockdemo);
  expect(mockdemo).toHaveBeenCalled();
});
